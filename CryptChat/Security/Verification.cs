using System;
using System.Text;
using System.Security.Cryptography;

namespace CryptChat.Security
{
  class Verification
  {
    public static bool Verify(string message, string signature, RSAParameters pubkey)
    {
        bool verified = false;
        using (var rsa = new RSACryptoServiceProvider())
        {
            byte[] todo = Encoding.ASCII.GetBytes(message);
            byte[] signed = Convert.FromBase64String(signature);
            try
            {
                rsa.ImportParameters(pubkey);
                SHA512Managed sha = new SHA512Managed();
                byte[] hash = sha.ComputeHash(signed);
                verified = rsa.VerifyData(todo, CryptoConfig.MapNameToOID("SHA512"), signed);
            }
            catch (CryptographicException e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                rsa.PersistKeyInCsp = false;
            }
        }

        return verified;
    }

    public static string Sign(string message, RSAParameters privkey)
    {
        byte[] signedbytes;
        using (var rsa = new RSACryptoServiceProvider())
        {
            byte[] orig = Encoding.ASCII.GetBytes(message);

            try
            {
                rsa.ImportParameters(privkey);
                signedbytes = rsa.SignData(orig, CryptoConfig.MapNameToOID("SHA512"));
            }
            catch (CryptographicException e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
            finally
            {
                rsa.PersistKeyInCsp = false;
            }
        }
        return Convert.ToBase64String(signedbytes);
    }
  }
}
