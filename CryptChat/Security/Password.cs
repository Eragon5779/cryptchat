﻿using System;
using System.Security.Cryptography;
using System.Text;
namespace CryptChat.Security.Password
{
    public class PasswordHash
    {
        public const int SaltByteSize = 24;
        public const int HashByteSize = 32; // to match the size of the PBKDF2-HMAC-SHA-1 hash
        public const int PBKDF2Iterations = 100000;
        public const int IterationIndex = 0;
        public const int SaltIndex = 1;
        public const int PBKDF2Index = 2;

        public static string HashPassword(string password, string salt_string)
        {
            var crypto_provider = new RNGCryptoServiceProvider();
            byte[] salt = Encoding.ASCII.GetBytes(salt_string);
            var hash = GetPBKDF2Bytes(password, salt, PBKDF2Iterations, HashByteSize);
            return PBKDF2Iterations + ":" + Convert.ToBase64String(hash);

        }

        public static string CreateSalt()
        {
            var crypto_provider = new RNGCryptoServiceProvider();
            byte[] salt = new byte[SaltByteSize];
            crypto_provider.GetBytes(salt);
            return Convert.ToBase64String(salt);
        }

        public static bool ValidatePassword(string password, string correct_hash)
        {
            char[] delimiter = { ':' };
            var split = correct_hash.Split(delimiter);
            var iterations = Int32.Parse(split[IterationIndex]);
            var salt = Convert.FromBase64String(split[SaltIndex]);
            var hash = Convert.FromBase64String(split[PBKDF2Index]);
            var testHash = GetPBKDF2Bytes(password, salt, iterations, hash.Length);
            return SlowEquals(hash, testHash);
        }

        private static bool SlowEquals(byte[] a, byte[] b)
        {
            var diff = (uint)a.Length ^ (uint)b.Length;
            for (int i = 0; i < a.Length && i < b.Length; i++)
            {
                diff |= (uint)(a[i] ^ b[i]);
            }
            return diff == 0;
        }

        public static byte[] GetPBKDF2Bytes(string password, byte[] salt, int iterations, int outputBytes)
        {
            var pbkdf2 = new Rfc2898DeriveBytes(password, salt);
            pbkdf2.IterationCount = iterations;
            return pbkdf2.GetBytes(outputBytes);
        }

    }
}
