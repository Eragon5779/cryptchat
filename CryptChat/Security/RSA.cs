using System;
using System.Text;
using System.IO;
using System.Linq;
using System.Security.Cryptography;

namespace CryptChat.Security {
  public class RSA
  {
      #region keys
      public static string[] Generate()
      {
          var csp = new RSACryptoServiceProvider(4096);
          var privkey = csp.ExportParameters(true);
          var pubkey = csp.ExportParameters(false);

          string pubkeystring;
          {
              var sw = new StringWriter();
              var xs = new System.Xml.Serialization.XmlSerializer(typeof(RSAParameters));
              xs.Serialize(sw, pubkey);
              pubkeystring = sw.ToString();
          }
          string privkeystring;
          {
              var sw = new System.IO.StringWriter();
              var xs = new System.Xml.Serialization.XmlSerializer(typeof(RSAParameters));
              xs.Serialize(sw, privkey);
              privkeystring = sw.ToString();
          }
          return new string[2] { privkeystring, pubkeystring };
      }

      public static RSAParameters FromString(string key, bool priv)
      {
          RSAParameters keyparams;
          using (var rsa = new RSACryptoServiceProvider())
          {
              rsa.FromXmlString(key);
              keyparams = rsa.ExportParameters(priv);
          }
          return keyparams;
      }

      public static string ReadPrivate(string password, string salt, string path)
      {
          string enc;
          try
          {
              enc = File.ReadAllText(path + "priv.pem");
          }
          catch (FileNotFoundException)
          {
              return "FNF";
          }
          return AES.Decrypt(enc, password, salt);
      }

      public static void SavePrivate(string password, string salt, string privkey, string path)
      {
          string enc = AES.Encrypt(privkey, password, salt);
          File.WriteAllText(path + "priv.pem", enc);
      }
      #endregion
      #region crypto
      public static string Encrypt(string message, RSAParameters rec_public)
      {
          byte[] encrypted;
          using (var rsa = new RSACryptoServiceProvider())
          {
              try
              {
                  rsa.ImportParameters(rec_public);
                  encrypted = rsa.Encrypt(Encoding.ASCII.GetBytes(message), true);
              }
              catch (CryptographicException e)
              {
                  Console.WriteLine(e.Message);
                  return null;
              }
              finally
              {
                  rsa.PersistKeyInCsp = false;
              }
          }
          return Convert.ToBase64String(encrypted);

      }

      public static string Decrypt(string enc, RSAParameters rec_private)
      {
          byte[] message;
          using (var rsa = new RSACryptoServiceProvider())
          {
              try
              {
                  rsa.ImportParameters(rec_private);
                  message = rsa.Decrypt(Convert.FromBase64String(enc), true);
              }
              catch (CryptographicException e)
              {
                  Console.WriteLine(e.Message);
                  return null;
              }
              finally
              {
                  rsa.PersistKeyInCsp = false;
              }
          }
          return Encoding.ASCII.GetString(message);
      }
      #endregion
  }
}
