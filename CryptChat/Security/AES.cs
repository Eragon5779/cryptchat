using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using CryptChat.Security.Password;

namespace CryptChat.Security {
    public class AES
    {

      private const string valid = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890?!@#$^&-_";
      public static Dictionary<string, string> Encrypt(string message, int keysize = 256)
      {
          // Need a message to encrypt
          if (String.IsNullOrEmpty(message))
          {
            throw new ArgumentNullException("message");
          }

          // The return data
          Dictionary<string, string> results = new Dictionary<string, string>();

          // Needed byte arrays
          byte[] vectorbytes = new byte[16];
          byte[] keybytes = new byte[keysize/8];
          byte[] cipherbytes;

          // Generate IV and Key
          using (RNGCryptoServiceProvider provider = new RNGCryptoServiceProvider())
          {
            provider.GetBytes(vectorbytes);
            provider.GetBytes(keybytes);
            results.Add("IV", Convert.ToBase64String(vectorbytes));
            results.Add("Key", Convert.ToBase64String(keybytes));
          }

          // AES logic
          using (RijndaelManaged aes = new RijndaelManaged())
          {
            aes.IV = vectorbytes;
            aes.Key = keybytes;
            ICryptoTransform enc = aes.CreateEncryptor(aes.Key, aes.IV);
            using (MemoryStream ms = new MemoryStream())
            {
              using (CryptoStream cs = new CryptoStream(ms, enc, CryptoStreamMode.Write))
              {
                using (StreamWriter sw = new StreamWriter(cs))
                {
                  sw.Write(message);
                }
                cipherbytes = ms.ToArray();
              }
            }
          }
          results.Add("Cipher",Convert.ToBase64String(cipherbytes));
          return results;
      }

      public static string Encrypt(string message, string password, string salt,
                                                  int iters = 100000, int keysize = 256)
      {
          if (String.IsNullOrEmpty(message))
          {
              return "";
          }

          byte[] vectorbytes = new byte[16];
          byte[] saltbytes = Encoding.ASCII.GetBytes(salt);
          using (SHA512 sha512 = new SHA512Managed())
          {
              vectorbytes = sha512.ComputeHash(saltbytes).Take(16).ToArray();
          }
          byte[] keybytes = PasswordHash.GetPBKDF2Bytes(password, saltbytes, iters, keysize / 8);
          byte[] cipherbytes;
          using (RijndaelManaged aes = new RijndaelManaged())
          {
              aes.IV = vectorbytes;
              aes.Key = keybytes;
              ICryptoTransform enc = aes.CreateEncryptor(aes.Key, aes.IV);
              using (MemoryStream ms = new MemoryStream())
              {
                  using (CryptoStream cs = new CryptoStream(ms, enc, CryptoStreamMode.Write))
                  {
                      using (StreamWriter sw = new StreamWriter(cs))
                      {
                          sw.Write(message);
                      }
                      cipherbytes = ms.ToArray();
                  }
              }
          }
          return Convert.ToBase64String(cipherbytes);
      }

      public static string Decrypt(Dictionary<string, string> data, int keysize = 256)
      {
        // Sanity check the data
        if (data.Count == 0)
        {
          throw new ArgumentNullException("data");
        }
        if (!data.ContainsKey("IV"))
        {
          throw new ArgumentNullException("data[\"IV\"]");
        }
        if (!data.ContainsKey("Key"))
        {
          throw new ArgumentNullException("data[\"Key\"]");
        }
        if (!data.ContainsKey("Cipher"))
        {
          throw new ArgumentNullException("data[\"Cipher\"]");
        }

        // Initialize necessary vars
        byte[] vectorbytes = Convert.FromBase64String(data["IV"]);
        byte[] keybytes = Convert.FromBase64String(data["Key"]);
        byte[] cipherbytes = Convert.FromBase64String(data["Cipher"]);
        string message;

        // AES logic
        using (RijndaelManaged aes = new RijndaelManaged())
        {
            aes.Key = keybytes;
            aes.IV = vectorbytes;
            ICryptoTransform dec = aes.CreateDecryptor(aes.Key, aes.IV);
            using (MemoryStream ms = new MemoryStream(cipherbytes))
            {
                using (CryptoStream cs = new CryptoStream(ms, dec, CryptoStreamMode.Read))
                {
                    using (StreamReader sr = new StreamReader(cs))
                    {
                        message = sr.ReadToEnd();
                    }
                }
            }
        }
        return message;

      }

      public static string Decrypt(string ciphertext, string password, string salt,
                                                  int iters = 100000, int keysize = 256)
      {
          if (String.IsNullOrEmpty(ciphertext))
          {
              return "";
          }
          byte[] vectorbytes = new byte[16];
          byte[] saltbytes = Encoding.ASCII.GetBytes(salt);
          using (SHA512 sha512 = new SHA512Managed())
          {
              vectorbytes = sha512.ComputeHash(saltbytes).Take(16).ToArray();
          }
          byte[] cipherbytes = Convert.FromBase64String(ciphertext);
          byte[] keybytes = PasswordHash.GetPBKDF2Bytes(password, saltbytes, iters, keysize / 8);
          string message;
          using (RijndaelManaged aes = new RijndaelManaged())
          {
              aes.Key = keybytes;
              aes.IV = vectorbytes;
              ICryptoTransform dec = aes.CreateDecryptor(aes.Key, aes.IV);
              using (MemoryStream ms = new MemoryStream(cipherbytes))
              {
                  using (CryptoStream cs = new CryptoStream(ms, dec, CryptoStreamMode.Read))
                  {
                      using (StreamReader sr = new StreamReader(cs))
                      {
                          message = sr.ReadToEnd();
                      }
                  }
              }
          }
          return message;
      }
  }
}
