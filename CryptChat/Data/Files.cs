﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;
using CryptChat.Security;
using System.Windows;

namespace CryptChat.Data
{
    public class Files
    {
        private static readonly App app = Application.Current as App;
        public static List<Chat> get_chats()
        {
            string chat_path = app.user.path + "\\chats.json";
            List<Chat> chats = null;
            if (!File.Exists(chat_path))
            {
                return chats;
            }

            string encrypted_chats = File.ReadAllText(chat_path);
            chats = JsonConvert.DeserializeObject<List<Chat>>(AES.Decrypt(encrypted_chats, app.user.get_pass(), app.user.salt));

            return chats;
        }
        public static List<Message> get_messages()
        {
            string msg_path = app.user.path + "\\messages.json";
            List<Message> msgs = null;
            if (!File.Exists(msg_path)) {
                return msgs;
            }

            string encrypted_msgs = File.ReadAllText(msg_path);
            string decrypted_msg_json = AES.Decrypt(encrypted_msgs, app.user.get_pass(), app.user.salt);
            msgs = JsonConvert.DeserializeObject<List<Message>>(decrypted_msg_json);

            return msgs;
        }

        public static bool write_chats(List<Chat> chats)
        {
            string chat_path = app.user.path + "\\chats.json";
            string json = JsonConvert.SerializeObject(chats, Formatting.Indented);
            string encrypted = AES.Encrypt(json, app.user.get_pass(), app.user.get_salt());
            try
            {
                File.WriteAllText(chat_path, encrypted);
            }
            catch {
                return false;
            }
            return true;
        }

        public static bool write_msgs(List<Message> msgs)
        {
            // Convert list of messages with decrypted to only API-storable data
            List<Message> safe_list = new List<Message>();
            foreach (Message m in msgs)
            {
                safe_list.Add(new Message(m._id, m.message, m.signature, m.timestamp, m.chat, m.sender, m.key_encrypted));
            }
            string msg_path = app.user.path + "\\messages.json";
            string json = JsonConvert.SerializeObject(safe_list, Formatting.Indented);
            string encrypted = AES.Encrypt(json, app.user.get_pass(), app.user.get_salt());
            try
            {
                File.WriteAllText(msg_path, encrypted);
            }
            catch
            {
                return false;
            }
            return true;
        }
    }
}
