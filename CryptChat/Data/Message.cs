﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace CryptChat.Data
{
    public class Message
    {
        public string _id { get; set; }
        public string chat { get; set; }
        public string message { get; set; }
        public string key_encrypted { get; set; }
        public string key { get; set; }
        public string IV { get; set; }
        public string signature { get; set; }
        public double timestamp { get; set; }
        public string decrypted { get; set; }
        public string sender { get; set; }
        public Message(string _id, string sender, string message, string decrypted, string signature, string key_encrypted, double timestamp = -1, string chat = null)
        {
            this._id = _id;
            this.decrypted = decrypted;
            this.signature = signature;
            this.timestamp = timestamp;
            this.chat = chat;
            this.message = message;
            this.sender = sender;
            this.key_encrypted = key_encrypted;
        }

        public Message(Dictionary<string, string> data)
        {
            this._id = data["_id"];
            this.signature = data["signature"];
            this.timestamp = Convert.ToDouble(data["timestamp"]);
            this.chat = data["chat"];
            this.message = data["message"];
            this.sender = data["sender"];
            this.key_encrypted = data["key"];
        }

        // Message format to be used explicitly for exporting
        [JsonConstructor]
        public Message(string _id, string message, string signature, double timestamp, string chat, string sender, string key_encrypted)
        {
            this._id = _id;
            this.signature = signature;
            this.timestamp = timestamp;
            this.chat = chat;
            this.message = message;
            this.sender = sender;
            this.key_encrypted = key_encrypted;
        }

        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        public Dictionary<string, string> GetDecryptDict()
        {
            return new Dictionary<string, string>
            {
                { "Cipher",message },
                { "IV",IV },
                { "Key",key }
            };
        }

    }
}
