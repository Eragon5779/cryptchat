﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace CryptChat.Data
{
    public class Chat
    {
        public List<string> members { get; set; }
        public int msg_count { get; set; }
        public string _id { get; set; }
        public double latest { get; set; }

        [JsonConstructor]
        public Chat(string _id, int msg_count = 0, List<string> members = null, double latest = -1)
        {
            this._id = _id;
            this.msg_count = msg_count;
            this.members = members;
            this.latest = latest;
        }

        public Chat(Dictionary<string, string> data)
        {
            this._id = data["_id"];
            this.msg_count = Convert.ToInt32(data["msg_count"]);
            this.members = data["members"].Split(',').ToList();
        }

        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }
    }
}
