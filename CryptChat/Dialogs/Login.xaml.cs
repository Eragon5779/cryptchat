﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using System.Diagnostics;

using CryptChat.Account.User;
using CryptChat.API.Handler;
using CryptChat.Security.Psec;
using CryptChat.Security.Ksec;
using CryptChat.Account.Registration;

namespace CryptChat.Dialogs
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : Window
    {
        public Login()
        {
            InitializeComponent();
        }

        private void BtnLogin_Click(object sender, RoutedEventArgs e)
        {
            string username = txtUsername.Text;
            string password = txtPassword.Password;
            string salt = Handler.GetSalt(username);
            string hash = PasswordHash.HashPassword(password, salt);
            if (Handler.Login(username, hash))
            {
                Dictionary<string, string> data = Handler.GetUser(username);
                App.user = new User(data, password, RSA.ReadPrivate(password, data["salt"], App.FILEPATH + $"{data["_id"]}\\"));
                if (App.user.get_privkey().Contains("FNF"))
                {
                    ShowErrorMessage("Authentication Error", "Failed to log in!\nPlease check that all info is correct\nand private key is in correct location.");
                    App.user = null;
                }
                else
                {
                    Close();
                }
            }
            else
            {
                ShowErrorMessage("Authentication Error", "Failed to log in!\nPlease check that all info is correct\nand private key is in correct location.");
            }
        }

        private void ShowErrorMessage(string title, string message)
        {
            MessageBox.Show(message, title, MessageBoxButton.OK, MessageBoxImage.Error);
        }

        private void ShowSuccessMessage(string title, string message)
        {
            MessageBox.Show(message, title, MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void BtnRegister_Click(object sender, RoutedEventArgs e)
        {
            string username = txtUsername.Text;
            string password = txtPassword.Password;
            if (Registration.register(App.FILEPATH, username, password))
            {
                Dictionary<string, string> data = Handler.GetUser(username);
                App.user = new User(data, password, RSA.ReadPrivate(password, data["salt"], App.FILEPATH + $"\\{data["_id"]}\\"));
                ShowSuccessMessage("Registration Success", $"Registered Successfully!\nWelcome to CryptChat {username}");
                Close();
            }
            else
            {
                ShowErrorMessage("Registration Failure", "Failed to register!\nIf this issue continues, please try a\ndifferent username.");
            }
        }
    }
}
