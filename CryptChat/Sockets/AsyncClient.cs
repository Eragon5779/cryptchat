﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using CryptChat.Data;

using Newtonsoft.Json;

namespace CryptChat.Sockets
{
    public class AsyncClient
    {

        private static readonly App app = (Application.Current as App);
        private readonly Handler handler = app.handler;
        private readonly EventWaitHandle waiter = new AutoResetEvent(false);
        private string hostname { get; set; }
        private int port { get; set; }
        private IPAddress ip;
        private TcpClient client = new TcpClient();
        private NetworkStream stream = default(NetworkStream);
        private Thread ctThread;
        private string readData = null;
        private bool stopped = false;

        public AsyncClient(string hostname, int port)
        {
            this.hostname = hostname;
            this.port = port;
            ip = Dns.GetHostAddresses(this.hostname)[0];
        }

        public bool Connected
        {
            get
            {
                return client.Connected;
            }
        }

        public void Connect()
        {
            client.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.KeepAlive, true);
            try
            {
                client.Connect(ip, port);
            }
            catch (SocketException)
            {
                Console.WriteLine("Server appears to be offline. Are you connected to the net?");
                return;
            }
            stream = client.GetStream();

            ctThread = new Thread(get_message);
            ctThread.Start();
            Console.WriteLine("Connected to server.");
        }

        private void get_message()
        {
            StreamObject test_parse = null;
            while (!stopped)
            {
                stream = client.GetStream();
                int buffsize = 0;
                string returnData = "";
                while (true)
                {
                    buffsize = client.ReceiveBufferSize - 1;
                    byte[] inStream = new byte[buffsize];
                    stream.Read(inStream, 0, buffsize);
                    returnData += Encoding.ASCII.GetString(inStream).TrimEnd((Char)0);
                    try
                    {
                        test_parse = JsonConvert.DeserializeObject<StreamObject>(returnData);
                        break;
                    } catch (JsonReaderException)
                    {
                        // Continue reading in data until data can be parsed
                    } catch (JsonSerializationException)
                    {
                        // Continue...
                    }
                }
                
                readData = "" + returnData;
                parse_data();
            }
        }

        public void send_message(SendObject so, EventWaitHandle e)
        {
            string jstr = JsonConvert.SerializeObject(so);
            byte[] outstream = Encoding.ASCII.GetBytes(jstr);
            stream.Write(outstream, 0, outstream.Length);
            stream.Flush();
            waiter.WaitOne();
            e.Set();
            waiter.Reset();
        }

        public void send_disconnect()
        {
            string jstr = "{\"connection\":\"close\"}";
            byte[] outstream = Encoding.ASCII.GetBytes(jstr);
            stream.Write(outstream, 0, outstream.Length);
            stream.Flush();
            stopped = true;
        }

        private void parse_data()
        {
            StreamObject so = JsonConvert.DeserializeObject<StreamObject>(readData);
            switch (so.type)
            {
                case "new_msg_list": update_msg_stack(so); break;
                case "new_msg": update_msg_stack(so); break;
                case "new_chat_list": update_chat_stack(so); break;
                case "new_chat": update_chat_stack(so); break;
                case "auth": handler.auth_stro = new ResponseObject(JsonConvert.DeserializeObject<Dictionary<string, string>>(JsonConvert.SerializeObject(so.data))); break;
                case "msg": handler.msg_stro = new ResponseObject(JsonConvert.DeserializeObject<Dictionary<string, string>>(JsonConvert.SerializeObject(so.data))); break;
                case "chat": handler.chat_stro = new ResponseObject(JsonConvert.DeserializeObject<Dictionary<string, string>>(JsonConvert.SerializeObject(so.data))); break;
                case "clist": handler.chat_clo = new ChatListObject(so); break;
                case "cmlist": handler.chat_mlo = new MessageListObject(so); break;
                default: Console.WriteLine(so.type); break;
            }
            waiter.Set();
        }

        private delegate void UpdateChatStackCallback();

        private void update_msg_stack(StreamObject so)
        {
            if (so.type == "new_msg")
            {
                MessageObject mo = new MessageObject(so);
                app.messages.Add(mo.GetItem());
            }
            if (so.type == "new_msg_list")
            {
                MessageListObject mlo = new MessageListObject(so);
                foreach (Message m in mlo.GetList())
                {
                    app.messages.Add(m);
                }
            }
        }

        private void update_chat_stack(StreamObject so)
        {
            if (so.type == "new_chat")
            {
                ChatObject mo = new ChatObject(so);
                app.chats.Add(mo.GetItem());
            }
            if (so.type == "new_chat_list")
            {
                ChatListObject mlo = new ChatListObject(so);
                foreach (Chat c in mlo.GetList())
                {
                    app.chats.Add(c);
                }
            }
        }
    }
}
