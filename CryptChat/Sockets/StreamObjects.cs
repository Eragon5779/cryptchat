﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

using CryptChat.Data;

namespace CryptChat.Sockets
{
    public class SendObject
    {
        public string path { get; set; }
        public string cmd { get; set; }
        public Dictionary<string, string> data { get; set; }
        public SendObject(string path, string cmd = null, Dictionary<string, string> data = null)
        {
            this.path = path;
            this.cmd = cmd;
            this.data = data;
        }
    }
    public class ResponseObject
    {
        public Dictionary<string, string> data;
        public ResponseObject(Dictionary<string, string> data)
        {
            this.data = data;
        }
    }
    public class StreamObject
    {
        public string type { get; set; }
        public object data { get; set; }
        public StreamObject(string type, object data)
        {
            this.type = type;
            this.data = data;
        }
    }
    public class MessageListObject
    { 

        public List<Message> data { get; set; }
        public MessageListObject(StreamObject so)
        {
            this.data = JsonConvert.DeserializeObject<List<Message>>(JsonConvert.SerializeObject(so.data));
        }

        public List<Message> GetList()
        {
            List<Message> retval = new List<Message>();
            foreach (Message m in data)
            {
                retval.Add(m);
            }
            return retval;
        }
    }
    public class MessageObject
    {
        public Dictionary<string, string> data { get; set; }

        public MessageObject(StreamObject so)
        {
            this.data = JsonConvert.DeserializeObject<Dictionary<string, string>>(JsonConvert.SerializeObject(so.data));
        }

        public Message GetItem()
        {
            return new Message(data);
        }
    }
    public class ChatListObject
    {
        public List<Chat> data { get; set; }

        public ChatListObject(StreamObject so)
        {
            this.data = JsonConvert.DeserializeObject<List<Chat>>(JsonConvert.SerializeObject(so.data));
        }

        public List<Chat> GetList()
        {
            List<Chat> retval = new List<Chat>();
            foreach (Chat c in data)
            {
                retval.Add(c);
            }
            return retval;
        }
    }
    public class ChatObject
    {
        public Chat data { get; set; }

        public ChatObject(StreamObject so)
        {
            this.data = JsonConvert.DeserializeObject<Chat>(JsonConvert.SerializeObject(so.data));
        }

        public Chat GetItem()
        {
            return data;
        }
    }
}
