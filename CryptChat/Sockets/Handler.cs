﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using CryptChat.Data;

namespace CryptChat.Sockets
{
    public class Handler
    {
        #region vars
        private App app;
        private static readonly EventWaitHandle waiter = new AutoResetEvent(false);
        private static SendObject auth_so = new SendObject("auth");
        private static SendObject msg_so = new SendObject("msg");
        private static SendObject chat_so = new SendObject("chat");
        public ResponseObject auth_stro { get; set; }
        public ResponseObject msg_stro { get; set; }
        public ResponseObject chat_stro { get; set; }
        public MessageListObject chat_mlo { get; set; }
        public ChatListObject chat_clo { get; set; }
        public Handler(App app)
        {
            this.app = app;
        }
        #endregion
        #region utility
        private readonly Dictionary<string, string> Offline = new Dictionary<string, string>
        {
            { "error", "server unreachable" }
        };
        public string FixPublic(string pub)
        {
            return $"<?xml version=\"1.0\" encoding=\"utf-16\"?>\n<RSAParameters xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n  <Exponent>AQAB</Exponent>\n  <Modulus>{pub}</Modulus>\n</RSAParameters>";
        }
        public string Strip_Public(string pub)
        {
            return pub.Split('>')[5].Split('<')[0];
        }
        #endregion
        #region auth
        
        public string GetSalt(string username)
        {
            string retval = "";
            auth_so.cmd = "getsalt";
            auth_so.data = new Dictionary<string, string> {
                { "username", username }
            };
            
            app.aclient.send_message(auth_so, waiter);
            waiter.WaitOne();
            waiter.Reset();
            if (auth_stro.data.ContainsKey("error"))
            {
                retval = auth_stro.data["error"];
            }
            else
            {
                retval = auth_stro.data["salt"];
            }
            auth_stro = null;
            return retval;
        }

        public Dictionary<string, string> GetUser(string username)
        {
            auth_so.cmd = "getuser";
            auth_so.data = new Dictionary<string, string>
            {
                { "username", username }
            };
            app.aclient.send_message(auth_so, waiter);
            waiter.WaitOne();
            waiter.Reset();
            SpinWait.SpinUntil(() => auth_stro != null);
            Dictionary<string, string> retval = auth_stro.data;
            auth_stro = null;
            if (retval.ContainsKey("public"))
            {
                retval["public"] = FixPublic(retval["public"]);
            }
            return retval;
        }
        public Dictionary<string, string> Login(string username, string hash)
        {
            auth_so.cmd = "login";
            auth_so.data = new Dictionary<string, string>
            {
                { "username", username },
                { "hash", hash }
            };
            app.aclient.send_message(auth_so, waiter);
            waiter.WaitOne();
            waiter.Reset();
            SpinWait.SpinUntil(() => auth_stro != null);
            Dictionary<string, string> retval = auth_stro.data;
            auth_stro = null;
            return retval;
        }

        public Dictionary<string, string> Register(string username, string hash, string salt, string pubkey)
        {
            auth_so.cmd = "register";
            auth_so.data = new Dictionary<string, string>
            {
                { "username", username },
                { "hash", hash},
                { "salt", salt},
                { "public", Strip_Public(pubkey)}
            };
            app.aclient.send_message(auth_so, waiter);
            waiter.WaitOne();
            waiter.Reset();
            SpinWait.SpinUntil(() => auth_stro != null);
            Dictionary<string, string> retval = auth_stro.data;
            auth_stro = null;
            return retval;
        }
        #endregion
        #region user
        // Deprecated, here for legacy reasons
        #endregion
        #region msg
        
        public Dictionary<string, string> Send_Message(string sender, string recipient, string message, string r_key, string s_key, string signature, string token)
        {
            msg_so.cmd = "sendmessage";
            msg_so.data = new Dictionary<string, string>
            {
                { "sender", sender },
                { "recipient", recipient },
                { "message", message },
                { "s_key", s_key },
                { "r_key", r_key },
                { "signature", signature },
                { "token", token }
            };
            app.aclient.send_message(msg_so, waiter);
            waiter.WaitOne();
            waiter.Reset();
            SpinWait.SpinUntil(() => msg_stro != null);
            Dictionary<string, string> retval = msg_stro.data;
            msg_stro = null;
            return retval;
        }
        #endregion
        #region chat
        
        public Dictionary<string, string> Get_Chat(string id, string token)
        {
            chat_so.cmd = "getchat";
            chat_so.data = new Dictionary<string, string>
            {
                { "_id", id },
                { "token", token }
            };
            app.aclient.send_message(chat_so, waiter);
            waiter.WaitOne();
            waiter.Reset();
            SpinWait.SpinUntil(() => chat_stro != null);
            Dictionary<string, string> retval = chat_stro.data;
            chat_stro = null;
            return retval;
        }

        public List<Message> Get_New(string token, string id, string oldest = "-1")
        {
            chat_so.cmd = "getnew";
            chat_so.data = new Dictionary<string, string>
            {
                { "token", token },
                { "chat_id", id },
                { "oldest", oldest }
            };
            app.aclient.send_message(chat_so, waiter);
            waiter.WaitOne();
            waiter.Reset();
            SpinWait.SpinUntil(() => chat_mlo != null);
            List<Message> retval = chat_mlo.GetList();
            chat_mlo = null;
            foreach (Message m in retval)
            {
                m.key_encrypted = m.key;
                m.key = null;
            }
            return retval;
        }

        public List<Chat> Get_All(string token)
        {
            chat_so.cmd = "getall";
            chat_so.data = new Dictionary<string, string>
            {
                { "token", token }
            };
            app.aclient.send_message(chat_so, waiter);
            waiter.WaitOne();
            waiter.Reset();
            SpinWait.SpinUntil(() => chat_clo != null);
            List<Chat> retval = chat_clo.GetList();
            chat_clo = null;
            return retval;
        }
        #endregion
    }
}
