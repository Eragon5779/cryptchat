﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

using CryptChat.Account.User;
using CryptChat.Data;
using CryptChat.Sockets;

namespace CryptChat
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        // Global vars
        public readonly string FILEPATH = $"C:\\Users\\{Environment.UserName}\\.cryptchat\\";
        public User user;
        public ObservableCollection<Chat> chats = new ObservableCollection<Chat>();
        public ObservableCollection<Message> messages = new ObservableCollection<Message>();
        public AsyncClient aclient;

        protected internal Handler handler;

        private void Application_Startup(object sender, StartupEventArgs e)
        {
            handler = new Handler(this);
            aclient = new AsyncClient("cryptchat.dragonfirecomputing.com", 55132);
            aclient.Connect();
            if (!System.IO.Directory.Exists(FILEPATH))
            {
                System.IO.Directory.CreateDirectory(FILEPATH);
            }
        }
    }
}