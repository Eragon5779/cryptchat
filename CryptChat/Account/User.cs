﻿using System;
using System.Collections.Generic;
using CryptChat.Sockets;
using CryptChat.Security;
using System.Windows;

namespace CryptChat.Account.User
{
    public class User
    {
        private static readonly App app = Application.Current as App;
        private readonly Handler handler = app.handler;
        public string _id;
        public string hash;
        public string pubkey;
        public string username;
        public string salt;
        public string path;
        private readonly string password;
        private string privkey;
        private string token;

        public User(string _id, string pubkey, string username)
        {
            this._id = _id;
            this.pubkey = pubkey;
            this.username = username;
        }

        public User(string _id, string hash, string pubkey, string username, string salt, string password, string token)
        {
            this._id = _id;
            this.hash = hash;
            this.pubkey = pubkey;
            this.username = username;
            this.salt = salt;
            this.password = password;
<<<<<<< HEAD
            this.path = app.FILEPATH + $"{this._id}";
            this.token = token;
=======
            this.path = App.FILEPATH + $"{this._id}";
>>>>>>> c1dda6c535310634023a3d85047c9be08fcf1616
        }
        public User(Dictionary<string, string> data, string password)
        {
            this._id = data["_id"];
            this.hash = data["hash"];
            this.pubkey = data["public"];
            this.username = data["username"];
            this.salt = data["salt"];
            this.password = password;
<<<<<<< HEAD
            this.path = app.FILEPATH + $"{this._id}";
=======
            this.path = App.FILEPATH + $"{this._id}";
>>>>>>> c1dda6c535310634023a3d85047c9be08fcf1616
        }

        public User(string _id, string hash, string pubkey, string username, string salt, string password, string privkey, string token)
        {
            this._id = _id;
            this.hash = hash;
            this.pubkey = pubkey;
            this.username = username;
            this.salt = salt;
            this.password = password;
            this.privkey = privkey;
<<<<<<< HEAD
            this.path = app.FILEPATH + $"{this._id}";
            this.token = token;
=======
            this.path = App.FILEPATH + $"{this._id}";
>>>>>>> c1dda6c535310634023a3d85047c9be08fcf1616
        }
        public User(Dictionary<string, string> data, string password, string privkey, string token)
        {
            this._id = data["_id"];
            this.hash = data["hash"];
            this.pubkey = data["public"];
            this.username = data["username"];
            this.salt = data["salt"];
            this.password = password;
            this.privkey = privkey;
<<<<<<< HEAD
            this.path = app.FILEPATH + $"{this._id}";
            this.token = token;
=======
            this.path = App.FILEPATH + $"{this._id}";
>>>>>>> c1dda6c535310634023a3d85047c9be08fcf1616
        }

        public void get_private_from_file(string path)
        {
            privkey = RSA.ReadPrivate(password, salt, path);
        }

        public string get_id()
        {
            return _id;
        }
        public string get_pass()
        {
            return password;
        }
        public string get_hash()
        {
            return hash;
        }
        public string get_pubkey()
        {
            return handler.FixPublic(pubkey);
        }
        public string get_username()
        {
            return username;
        }
        public string get_salt()
        {
            return salt;
        }
        public string get_privkey()
        {
            return privkey;
        }
        public string get_token()
        {
            return token;
        }
    }
}
